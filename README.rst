Obsolete
--------

This project is obsolete as setup.py has been deprecated for several years and the ecosystem is moving to declarative configuration. Many of the problems that reusable setup.py fragments were designed to address are now handled in other ways anyway.

An Unfortunate Situation
------------------------

``setup.py`` has long been a blight upon the Python landscape.  With no
disrespect intended to the tireless maintainers of ``distutils``, ``setuptools``,
``pip``, ``PyPI``, ``virtualenv``, ``wheel``, and the thicket of other tools you need
to make even basic installation operations succeed in a modern Python
environment, the situation is pretty bad.

It's *particularly* bad if you need to actually do something interesting during
your build process.  For example, if you need to determine the version of the
software being bundled for installation.  The current recommendation for doing
this is --- and I’m not kidding here --- discover your version by `copying and
pasting a bunch of regular expressions
<https://github.com/pypa/sampleproject/blob/bdd29f389096f450d4bd699ddb829c8907b8bca9/setup.py#L11>`_
*into every new project's ``setup.py`` file*.

With Reasonable Causes
======================

``setup.py`` runs in a variety of unpredictable contexts.  Its job is to be run
before you import things, so that you *can* import things.  So it's somewhat
understandable that you wouldn't be able to import things from it.  It might be
unfixable.

Prevailing opinion seems to be that the dynamic, executable nature of
``setup.py`` might have been a mistake anyway, and that the future will be some
sort of entirely static metadata.

Enter Meta-Setup
================

Nevertheless, sometimes you want to write some code, carefully test it, develop
it along with your project, and then run it in your `setup.py` at either
install or build time.  It's reasonable to impose *some* restrictions upon this
sort of early-stage bootstrapping code, but having to put it all into one giant
fused heap in `setup.py` is not good for anyone.

`metasetup` is a tool for building a `setup.py` out of pieces; pieces which you
can develop and test as (almost) normal Python modules, but which are
"statically linked" by being copied as a unit into your `setup.py` in a
discrete build step.  You can then check this generated file into your
version-control repository, so that `pip install https://.../yourproject.git`
continues to work, but not have to maintain manual metadata.
