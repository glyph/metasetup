
def at_metasetup_time(static_metadata, parameters):
    objectName = parameters['from']
    from twisted.python.reflect import namedAny
    static_metadata.update(version=namedAny(objectName))
    return static_metadata
