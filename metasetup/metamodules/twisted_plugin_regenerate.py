
"""
Regenerate the plugin cache.
"""

def at_setup_time(static_metadata, parameters):
    from setuptools.command.install import install as Install
    class InstallAndRegenerate(Install):
        def run(self):
            """
            Runs the usual install logic, then regenerates the plugin cache.
            """
            Install.run(self)
            # install_requires means we already have twisted at this point,
            # right?
            from twisted import plugin
            from twisted.python.reflect import namedAny
            for pluginPackageName in parameters['plugin_packages']:
                pluginPackage = namedAny(pluginPackageName)
                list(plugin.getPlugins(plugin.IPlugin, pluginPackage))
    static_metadata.setdefault("cmdclass", {}).update(
        install=InstallAndRegenerate,
    )
    return static_metadata

def at_metasetup_time(static_metadata, parameters):
    from pkg_resources import Requirement
    if "Twisted" not in [Requirement.parse(x).project_name
                         for x in static_metadata.get('install_requires', ())]:
        static_metadata.setdefault('install_requires', []).append('Twisted')
    return static_metadata
