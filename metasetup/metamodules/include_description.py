
def at_metasetup_time(static_metadata, params):
    filename = params['from']
    with open(filename) as f:
        static_metadata.update(description=f.read())
    return static_metadata
