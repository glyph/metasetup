
def at_metasetup_time(static_metadata, parameters):
    from twisted.python.dist import getPackages
    package_list = static_metadata.setdefault("packages", [])
    for toplevel in parameters['top']:
        package_list.extend(getPackages(toplevel))
    return static_metadata

